if (process.env.NODE_ENV == "development") {
    require("./devhtml/index.html");
    require("./index.css");

    require("./libs/easeljs-0.8.2.combined.js");
    require("./libs/tweenjs-0.6.2.combined.js");
    require("./libs/hammer.min.js");
    require("./libs/jquery-3.0.0.js");
}

// React modules.
const React = require('react');
const ReactDOM = require('react-dom');

const App = require("./components/App");

// Additional export for testing purposes
window.React = React;

// Rendering of the app
ReactDOM.render((
    <App/>
), document.getElementById("app") );