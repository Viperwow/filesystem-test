const React = require("react");
const ReactDOM = require('react-dom');
const PT = React.PropTypes;

const styles = require("./App.css");
const cNames = require("classnames");

//region - Elements
const Filesystem = require("./widgets/Filesystem");
//endregion

class App extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.displayName = 'App';
    }

    render() {
        return (
            <div className={cNames(styles.root)}>
                <h1>Filesystem</h1>
                <Filesystem dropMessage="Брось сюда файлы"/>
            </div>
        );
    }
}

module.exports = App;