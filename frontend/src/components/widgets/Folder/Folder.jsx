const React = require("react");
const ReactDOM = require('react-dom');
const PT = React.PropTypes;

const folderClose = require("./icons/folder.svg");
const folderOpen = require("./icons/folder_open.svg");

const styles = require("./styles/Folder.css");
const cNames = require("classnames");

const File = require("../File");

//region - Private methods
const createFolderIcon = (isCollapsed) => {
    return (
        <img className={cNames(styles.icon)} src={isCollapsed ? folderClose : folderOpen} />
    );
};

const createFolderName = (name) => {
    if(name || name == 0) {
        return (
            <div className={cNames(styles.name)}>
                {name}
            </div>
        );
    }
};

const createChildren = (children) => {
    if(children) {
        return (
            children.map((child, index) => {
                return (
                    <li key={index.toString()}>
                        {child}
                    </li>
                );
            })
        );
    }
};
//endregion

class Folder extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.displayName = 'Folder';
    }

    state = {
        isCollapsed: true
    };

    static defaultProps = {
        name: "",
        callback: () => {}
    };

    static propTypes = {
        name: PT.string,
        callback: PT.func
    };

    componentDidMount = () => {
        ReactDOM.findDOMNode(this).addEventListener("click", (e) => {
            e.preventDefault();
            e.stopPropagation();

            this.setState({
                isCollapsed: !this.state.isCollapsed
            });
        });

        this.props.callback();
    };

    render() {
        return (
            <ul className={cNames(styles.folder)}>
                {createFolderIcon(this.state.isCollapsed)}
                {createFolderName(this.props.name)}
                {this.state.isCollapsed ? "" : createChildren(this.props.children)}
            </ul>
        );
    }
}

module.exports = Folder;