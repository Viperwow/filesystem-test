const React = require("react");
const ReactDOM = require('react-dom');
const PT = React.PropTypes;

const styles = require("./styles/Filesystem.css");
const cNames = require("classnames");

const File = require("../File");
const Folder = require("../Folder");

//region - Private methods
const dataTransferItemsToReact = (dataTransferItems, context) => {
    let entryArray = [];
    let i, length = dataTransferItems.length;
    for (i = 0; i < length; i++) {
        entryArray.push(dataTransferItems[i].webkitGetAsEntry());
    }

    entriesToReact(entryArray).then((result) => {
        context.setState({
            isDraggingOver: false,
            filesystem: result
        });
    });
};

const entriesToReact = (entries) => {
    return new Promise((resolve) => {
        let promiseOutput = entries.map((entry, index) => {
            let output;

            if (entry.isFile) {
                output = new Promise((resolve) => {
                    entry.getMetadata((metadata) => {
                        resolve(<File key={index.toString()} name={entry.name} size={metadata.size}/>);
                    });
                });
            } else if(entry.isDirectory) {
                output = new Promise((resolve) => {
                    entry.createReader().readEntries((folderEntries) => {
                        entriesToReact(folderEntries).then((children) => {
                            resolve(<Folder key={index.toString()} name={entry.name} children={children}/>);
                        });
                    });
                })
            }

            return output;
        });

        Promise.all(promiseOutput).then((output) => {
            resolve(output);
        });
    });
};

const createDropzone = (dropMessage) => {
    return (
        <div className={cNames(styles.dropzone)}>
            <p>{dropMessage}</p>
        </div>
    );
};
//endregion

class Filesystem extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.displayName = 'Filesystem';
    }

    componentDidMount = () => {
        let filesystemDOMNode = ReactDOM.findDOMNode(this);

        filesystemDOMNode.addEventListener('dragover', (e) => {
            e.stopPropagation();
            e.preventDefault();

            this.setState({
                isDraggingOver: true
            })
        }, false);

        filesystemDOMNode.addEventListener('drop', (e) => {
            e.stopPropagation();
            e.preventDefault();

            dataTransferItemsToReact(e.dataTransfer.items, this);
        }, false);

        filesystemDOMNode.addEventListener('dragleave', (e) => {
            e.stopPropagation();
            e.preventDefault();

            this.setState({
                isDraggingOver: false
            })
        }, false);
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        return this.state.isDraggingOver != nextState.isDraggingOver;
    };

    state = {
        isDraggingOver: false,
        filesystem: []
    };

    static defaultProps = {
        dropMessage: "Please, drag'n'drop files from the desktop!"
    };

    static propTypes = {
        dropMessage: PT.string
    };

    render() {
        return (
            <div className={cNames(styles.filesystem)}>
                { this.state.isDraggingOver || this.state.filesystem.length == 0 ? createDropzone(this.props.dropMessage) : this.state.filesystem }
            </div>
        );
    }
}

module.exports = Filesystem;