const React = require("react");
const ReactDOM = require('react-dom');
const PT = React.PropTypes;

const file = require('./icons/file.svg');

const styles = require("./styles/File.css");
const cNames = require("classnames");

//region - Private methods
const createFileIcon = () => {
    return (
        <img className={cNames(styles.icon)} src={file} />
    );
};

const createFileName = (name) => {
    if(name || name == 0) {
        return (
            <div className={cNames(styles.name)}>
                {name}
            </div>
        );
    }
};

const createFileSize = (size) => {
    if(size || size == 0) {
        return (
            <div className={cNames(styles.size)}>
                ({size})
            </div>
        );
    }
};
//endregion

class File extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.displayName = 'File';
    }

    static defaultProps = {
        name: "",
        size: 0,
        callback: () => {}
    };

    static propTypes = {
        name: PT.string,
        size: PT.oneOfType([
            PT.string,
            PT.number
        ]),
        callback: PT.func
    };

    componentDidMount = () => {
        ReactDOM.findDOMNode(this).addEventListener("click", (e) => {
            e.preventDefault();
            e.stopPropagation();
        });

        this.props.callback();
    };

    render() {
        return (
            <div className={cNames(styles.file)}>
                {createFileIcon()}
                {createFileName(this.props.name)}
                {createFileSize(this.props.size >= 0 ? this.props.size : 0)}
            </div>
        );
    }
}

module.exports = File;