const path = require('path');
const webpack = require('webpack');

const WP_HOST = process.env.WP_HOST || 'localhost';
const WP_PORT = process.env.WP_PORT || '8080';
const WP_PROXY_HOST = process.env.WP_PROXY_HOST || 'localhost';
const WP_PROXY_PORT = process.env.WP_PROXY_PORT || '9090';

const PATHS = {
	root: path.resolve(__dirname + '/../'),
	src: path.resolve(__dirname, './src/'),
	libs: path.resolve(__dirname, './src/libs')
};

const projectName = path.parse(PATHS.root).name;
console.log(`project: ${projectName}`);

module.exports = {
	context: PATHS.src,
	devtool: 'inline-source-map',
	devServer: {
		host: WP_HOST,
		port: WP_PORT,
		historyApiFallback: true,
		proxy: [{
			path: `/${projectName}/backend/!*`,
			target: {
				host: WP_PROXY_HOST,
				port: WP_PROXY_PORT
			}
		}]
	},
	debug: true,

	entry: ['./index.jsx'],
	output: {
		path: path.join(PATHS.root, '/build/development/'),
		filename: '[name].js',
		publicPath: 'http://' + WP_HOST + ':' + WP_PORT + '/'
	},
	resolve: {
		extensions: ['', '.jsx', '.css', '.js', '.json', '.md', '.styl', '.less']
	},
	module: {
		loaders: [
			{
				test: /\.(js|jsx)$/,
				cacheDirectory: true,
				loader: 'babel-loader',
				include: PATHS.src,
				exclude: PATHS.libs
			}, {
				test: /\.js$/,
				loader: 'file-loader?name=/assets/js/[name].[ext]',
				include: PATHS.libs
			}, {
				test: /\.html$/,
				loader: 'file-loader?name=/[name].[ext]',
				include: PATHS.src
			}, {
				test: /\.css$/,
				loader: 'style-loader?sourceMap' +
				'!css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]_[local]_[hash:base64:5]' +
				'!postcss-loader',
				include: PATHS.src
			}, {
				test: /\.styl$/,
				loader: 'style-loader?sourceMap' +
				'!css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]_[local]_[hash:base64:5]' +
				'!postcss-loader!stylus-loader?resolve url',
				include: PATHS.src
			}, {
				test: /\.less$/,
				loader: 'style-loader?sourceMap' +
				'!css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]_[local]_[hash:base64:5]' +
				'!postcss-loader!less-loader',
				include: PATHS.src
			}, {
				test: /\.(ttf|eot|woff|woff2|png|ico|jpe?g|gif|svg)$/i,
				loader: 'file-loader?name=/assets/[ext]/[name].[hash:10].[ext]',
				include: PATHS.src
			}
		]
	},
	postcss: function() {
		return [
			require('oldie')(),
			require('postcss-cssnext')()
		]
	},
	externals: {
		'createjs': 'createjs',
		'hammer': 'Hammer',
		'jquery': 'jQuery'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('development')
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'common'
		})
	]
};


